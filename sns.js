
var util = require('util');
var AWS = require('./node_modules/aws-sdk');

var metadata = new AWS.MetadataService();
var instance_public_ipv4 = null;
var _config;
var _port;

module.exports = function sns(config, port) {
  _config = config;

  _port = port;
  AWS.config.update({region: config.region});

  metadata.request('/latest/meta-data/public-ipv4', get_public_ipv4);
};

function sns_subscribe(this_TopicArn, instance_public_ipv4) {
  var sns = new AWS.SNS();
  var instance_url =  "http://" + instance_public_ipv4 + ":" + _port + "/";
  console.log("sns end point url: " + instance_url);

  sns.subscribe(
	params = {
	  TopicArn: this_TopicArn, 
	  Protocol: "http", 
	  Endpoint: instance_url
	}, 
	function(err, data) { if (err) { console.log('Subscribe error: ' + err);} }
	);
}

function sns_confirm(request, data) {
  var sns = new AWS.SNS();
  var confirmation_body_json = JSON.parse(data);
  console.log('Confirming TopicArn: ' + confirmation_body_json.TopicArn + ', Token: ' + confirmation_body_json.Token);
  sns.confirmSubscription(params={"TopicArn": confirmation_body_json.TopicArn, "Token": confirmation_body_json.Token}, function(err, data) {console.log(err);} );
}

function sns_process(request, data, generator) {
  var payload = JSON.parse(data)
  //console.log(util.inspect(payload));

  // Interpret and update based upon data payload...
  if (payload.Subject && payload.Message) {
    var subject = payload.Subject;
    var lines = payload.Message.split("\n");

    for (var i=0; i<lines.length; i++) {
      var splits = lines[i].split(":", 2);
	    console.log("0:" + splits[0] + "; 1:" + splits[1] );

    	if (splits[0] == "sleep") {
        console.log("sns sleep");
    	} else if (splits[0] == "seed") {
         console.log("sns seed");
    	} else if (splits[0] == "skew") {
         console.log("sns skew: " + splits[1]);
         generator.skewRating(splits[1]);
    	}
    }
  }
};

function get_public_ipv4(err, instance_public_ipv4) {
  if (err == null) {
    sns_subscribe(_config.snsTopic, instance_public_ipv4);
  } else {
    console.log("Cannot get instance's public ip; cannot subscribe to SNS");
  }
}

function process_sns_cnc(request, generator) {
  if (request.headers['x-amz-sns-message-type'] == 'SubscriptionConfirmation') {
    var data = "";
    request.on('data', function(chunk) { data+= chunk; });
    request.on('end', function() { sns_confirm(request, data); });
  } else if (request.headers['x-amz-sns-message-type'] == 'Notification') {
    var data = "";
    request.on('data', function(chunk) { data+= chunk; });
    request.on('end', function() { sns_process(request, data, generator); });
  }
}

module.exports.process_sns_cnc = function(request, generator) {
        return process_sns_cnc(request,generator);
    }

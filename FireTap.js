// General includes
//var sns       = require('./sns.js');
//var AWS = require('aws-sdk');
var Util = require('util');

var EventEmitter = require('events').EventEmitter;
var sleep = require('sleep');

module.exports = FireTap;
Util.inherits(FireTap, EventEmitter)

function FireTap(generator,pool) {
  EventEmitter.call(this);

  this._poolCount = 20; 
  this._putCount = 0;
  this._putDelta = 0;
  this._runningCount = 0;

  this._generator = generator;
  console.log("generator = "+generator);
  this._pool = pool;
};

FireTap.prototype.run = function() {
  for (var i = 0; i < this._poolCount; i++) {
    this.prepareRecord();
  }
};


FireTap.prototype.getNext = function() {
  this.prepareRecord();
};

FireTap.prototype.startFireTapStats = function() {
  var a = this;
  setInterval(function() {
    a._putDelta = a._putCount;
 }, 1000);
};

FireTap.prototype.startGeneratorStats = function() {
  var a = this;
  setInterval(function() { a._generator.stats(a._generator) }, 5000);
};

FireTap.prototype.startPoolStats = function () {
  var a = this;
  setInterval(function() { a._pool.stats(a._pool) }, 5000);
}


FireTap.prototype.prepareRecord = function() {
  this._runningCount++;
  var a = this;
  this._pool.getConnection(function (err, kinesis) {
    if (err) {
      console.warn("Error: "+err);
    } else {
      var msg = a._generator.msg();
      var guid = a._generator.createGuid();
      var retryCount = 1;

      //a.putRecord(kinesis,msg,guid,retryCount,a.putRecord);
      kinesis.putRecord("DataPipelineDemo",  new Buffer(JSON.stringify(msg)).toString('base64'), guid, function (err, data) {
        a._putCount++;

        if(err && err.code == "ProvisionedThroughputExceededException") {
          console.log(err);
          sleep.sleep(5);
        } else if (err) {
          console.warn("Error: "+err);
        }
      });
      
    }
    a._runningCount--;
    a.getNext();
  });
};

FireTap.prototype.putRecord = function(kinesis,msg,guid,retryCount,callback) {
  var a = this;
  kinesis.putRecord("DataPipelineDemo",  new Buffer(JSON.stringify(msg)).toString('base64'), guid, function (err, data) {
    a._putCount++;

    if(err && err.code == "ProvisionedThroughputExceededException") {
      console.log(err);
      retryTimeout = (Math.pow(2,retryCount)*1000) + (Math.round(Math.random() * 1000));

      console.log("retry timeout, " + retryTimeout + ", retry iteration " + retryCount);
      retryCount++;
      setTimeout(callback(kinesis,msg,guid,retryCount,callback),retryTimeout);
      //sleep.sleep(retryTimeout);
      //sleep.sleep(1);
    } else if (err) {
      console.warn("Error: "+err);
    }
  }); 
};



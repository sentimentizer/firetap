var AWS = require('aws-sdk');   // Ensure we have the aws-sdk module installed
var Util = require('util');
var EventEmitter = require('events').EventEmitter;

module.exports = Kinesis;
Util.inherits(Kinesis, EventEmitter)

function Kinesis(pool, options) {
	EventEmitter.call(this);
	AWS.config.update({accessKeyId: options.accessKeyId, secretAccessKey: options.secretAccessKey, region: options.region});

	this._pool = pool;
	this._kinesis = new AWS.Kinesis();
	this._options = options;
	this._active = false;	// Not being used for a query
	this._batch = [];
}

Kinesis.prototype.putRecord = function(StreamName, Data, PartitionKey, cb) {
	this._active = true;

	var a = this;
	this._kinesis.putRecord({StreamName: StreamName, Data: Data, PartitionKey: PartitionKey}, function (err, data) {
		a._active = false;
		a._pool.releaseConnection(a); // sends data
		return cb(err, data);
    });
};
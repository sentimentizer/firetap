// General includes
var os        = require('os');
var cluster   = require('cluster');
var FireTap = require('./FireTap.js');
var http      = require('http');
var https     = require('https');
var Generator = require('./synthetic_generator.js');
var Pool = require('./Pool');

// Include AWS SDK and set credentials
var AWS_STS = require('aws-sdk');
var AWS_INSTANCE = require('aws-sdk');

// **** Check command line arguments first ****
var argv = require('optimist')
        .usage('Generate load into Kinesis.\nUsage: $0 --config=<file>')
        .describe('config', 'JSON Configuration file')
        .argv;

var config = require('./'+(argv.config ? argv.config : "config.json")); // Loads Configuration (prepend ./ so it reads the JSON file, not try and load a module)

// Expected config file elements
var configKeys = ["accessKeyId", "secretAccessKey", "region", "snsTopic", "useInstanceRoleToAssume"];

var errorFlag = false;
for (configKey in configKeys) {
  if (! config.hasOwnProperty(configKeys[configKey])) {
      console.error("expected "+configKeys[configKey]+" in config file!");
      errorFlag = true;
  }
}

if (errorFlag) {
  return;
}

/*** UPDATE THIS TO CHANGE THE SKEW ***/
var defaultSkew = 1;
var fireTap = null

if(config.useInstanceRoleToAssume == true) {
    instanceRoleAssume();
} else {
  main();
}

function main() {
  var generator = new Generator(-5, 5, defaultSkew);
  var poolCount = 50;
  var pool = new Pool(config);
  
  // set the global agent max sockets and assign to AWS object
  https.globalAgent.maxSockets = poolCount;
  var httpsAgent = https.globalAgent;
  AWS_INSTANCE.config.httpOptions.agent = httpsAgent;
 
  fireTap = new FireTap(generator,pool);

  // ******************************
  // Configure workers
  // ******************************
  var numCPUs = os.cpus().length;
  //var numCPUs = 1;
  if (cluster.isMaster) {
      var _sns = require('./sns');
      console.log("topicArn ("+config.snsTopic+")");
      _sns.control_TopicArn = config.snsTopic;
      var sns = new _sns(config,"8888");

     // Fork workers.
     for (var i = 1; i < numCPUs; i++) {
       cluster.fork();
     }
    
     cluster.on('exit', function(worker, code, signal) {
       console.log('worker ' + worker.process.pid + ' died');
     });

     http.createServer(function(request, response) {
      response.writeHead(200, {"Content-Type": "text/plain"});

      _sns.process_sns_cnc(request,generator);
     
      response.end();
    }).listen(process.env.PORT || 8888);

   } 
   else {
    if(cluster.isWorker)
        console.log(cluster.worker.id);

    var _sns = require('./sns');
    console.log("topicArn ("+config.snsTopic+")");
    _sns.control_TopicArn = config.snsTopic;
    var subscriptionPort = 8888 + cluster.worker.id;
    var sns = new _sns(config,subscriptionPort);

    //port = 8888 + workerid + 1;
    http.createServer(function(request, response) {
      response.writeHead(200, {"Content-Type": "text/plain"});

      _sns.process_sns_cnc(request,generator);
     

      response.end();
    }).listen(process.env.PORT || subscriptionPort);
  }

  fireTap.startFireTapStats();
  fireTap.startGeneratorStats();
  fireTap.startPoolStats();

  fireTap.run();

}

/*
//QUESTION: When I put run into FireTap class the connections build to 100 and only 20 messages send
function run () {
  if (fireTap._runningCount < fireTap._poolCount) {
    fireTap.putRecord();
    run();
  } else { // Slow down
    setTimeout(run, 10);
  }
};
*/

function instanceRoleAssume() {
  var ec2MetadataCreds = new AWS_INSTANCE.EC2MetadataCredentials();
  ec2MetadataCreds.refresh(function (err) {
    if (err) {
           console.log(err);
    } else {
    AWS_INSTANCE.config.update( {accessKeyId: ec2MetadataCreds.accessKeyId, secretAccessKey: ec2MetadataCreds.secretAccessKey, sessionToken: ec2MetadataCreds.sessionToken, region: config.region} );
    console.log("instanceRole(): accessKeyId("+ec2MetadataCreds.accessKeyId+") secret("+ec2MetadataCreds.secretAccessKey+") token("+ec2MetadataCreds.sessionToken+")");
    assumeRole();
   }
  });
}

function assumeRole () {
  var sts = new AWS_STS.STS();
  assumedRoleRequest = sts.assumeRole( { RoleArn: "arn:aws:iam::581371153492:role/SentimentizerCrossAccountKinesis",RoleSessionName: "SentimentizerCrossAccountKinesis", DurationSeconds: 900}, function(err, data) {
  if(err) {
         console.err(util.inspect(err));
  } else {
         AWS_STS.config.update({accessKeyId: data.Credentials.AccessKeyId, secretAccessKey: data.Credentials.SecretAccessKey, sessionToken: data.Credentials.SessionToken });
         AWS_STS.config.credentials.expireTime = data.Credentials.Expiration;
	 console.log("assumeRole(): accessKeyId("+data.Credentials.AccessKeyId+") secret("+data.Credentials.SecretAccessKey+") token("+data.Credentials.SessionToken+")");
         main();
  }
  });
 };

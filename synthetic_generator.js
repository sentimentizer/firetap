var util      = require("util");

module.exports = Generator;

function Generator(minRating, maxRating, skewMethod) {
	this._minRating = minRating;
	this._maxRating = maxRating;
	this._raitingCount = Math.abs(minRating) + Math.abs(maxRating)+1;

	this._skewDirection = null;
	this._skewMethod = skewMethod;

	this._votingList = new Object();
	this._weightedValues = Array();
	this._weightedValuesNorm = Array();

	if(skewMethod == 1) {
		this.generateWeight(this._raitingCount,null);
	} else {
			this._ratingSkewFactor = 1;
	}
};
	
Generator.prototype.msg = function() {
	var partkey = this.createGuid();
	var date = new Date();
	var millisecondsSinceEpoc = date.getTime();

	var payload = {
     topic : "TopicA", // TODO: Obtain randomly from prepopulated array of topics
     timestamp : millisecondsSinceEpoc,
     rating : this.getRandomRating(this._ratingSkew),
     useragent : "Mozilla/5.0 (compatible; MSIE 9.0; AOL 9.7; AOLBuild 4343.19; Windows NT 6.1; WOW64; Trident/5.0; FunWebProducts)" //TODO: Obtain randomly from prepopulated array of useragents
    }; 

    return payload;
};

Generator.prototype.getRandomNumber = function(min,max) {
	var random = Math.floor(Math.random() * (max - min + 1)) + min;
	return random;
};

Generator.prototype.getRandomRating = function() {	
	var randomRaiting = null;
	if(this._skewMethod == 1) {
		var needle = Math.random();
	    var high = this._weightedValuesNorm.length - 1;
	    var low = 0;
	    var weightedVote = null;
	    var remappedWeightedVote = null;
	    
	    while(low < high){
	        probe = Math.ceil((high+low)/2);
	        
	        if(this._weightedValuesNorm[probe] < needle){
	            low = probe + 1;
	        }else if(this._weightedValuesNorm[probe] > needle){
	            high = probe - 1;
	        }else{
	            return probe;
	        }
	    }
	    
	    if(low != high ){
	        weightedVote = (this._weightedValuesNorm[low] >= needle) ? low : probe;
	    }else{
	        weightedVote = (this._weightedValuesNorm[low] >= needle) ? low : low + 1;
	    }

	    randomRaiting = this._minRating + weightedVote;

	} else {
		var weighted = Math.pow(Math.random(), this._ratingSkewFactor);
		var scaled = Math.floor(weighted * (this._maxRating - this._minRating + 1)) + this._minRating;

		randomRaiting=scaled;
	}

	var total = this._votingList[randomRaiting]+1;
	if(!total) {
		total = 1;
	}  
	this._votingList[randomRaiting] = total;

	return randomRaiting;
}

Generator.prototype.generateWeight = function (ratingCount,skewDirection) {
	this._weightedValues = new Array(ratingCount);
	var increment = 10;
	var currentWeight = 0;
	var sum = 0;

	for(i = 0; i < ratingCount; i++) {
		if(skewDirection == "right" || skewDirection == "left") {
			if(skewDirection == "right") {
				this._weightedValues[i]=currentWeight;
			} else if(skewDirection == "left"){
				this._weightedValues[ratingCount-i-1]=currentWeight;
			}

			if(i % 2) {
				currentWeight+=increment;
			} 
		} else {
			this._weightedValues[i]=increment;
		}
	}

	// Normalize
	for (i=0; i<ratingCount; i++){
	    sum += this._weightedValues[i];
	    this._weightedValuesNorm[i] = sum;
	}

	for (i=0; i<ratingCount; i++){
	    this._weightedValuesNorm[i] = this._weightedValuesNorm[i]/sum;
	}
};

Generator.prototype.stats = function (generator) {
	console.log("generator distribution: " + util.inspect(this._votingList));
};

Generator.prototype.createGuid = function() {   
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    	var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
    	return v.toString(16);
	});
};

Generator.prototype.skewRating = function(skewDirection) {   
	if(this._skewMethod == 1) {
		this.generateWeight(this._raitingCount,skewDirection);
	} else {
		switch(skewDirection) {
			case "right":
				this._ratingSkewFactor = 0.1;
				break;
			case "left":
				this._ratingSkewFactor = 2;
				break;
			default:
				this._ratingSkewFactor = 1;
		}
	}
	
	this._skewDirection = skewDirection;
	console.log("Vote rating skew set to: " + this._skewDirection);
};


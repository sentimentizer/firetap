var Kinesis = require('./KinesisConnection');   // Ensure we have the aws-sdk module installed
var Util = require('util');
var EventEmitter = require('events').EventEmitter;

module.exports = Pool;
Util.inherits(Pool, EventEmitter)

function Pool(options) {
  EventEmitter.call(this);
  this.options = options;

  this.config = {}
  this.config.connectionLimit = 100;
  this.config.queueLimit = 5000;
  this.config.waitForConnections = true;

  this._allConnections   = [];
  this._freeConnections  = [];
  this._connectionQueue  = [];
  this._closed           = false;
  this._count = 0;
}

Pool.prototype.stats = function (pool) {
	console.log("all ("+pool._allConnections.length+") free("+pool._freeConnections.length+") queue("+pool._connectionQueue.length+") count("+pool._count+")");
}

Pool.prototype.getConnection = function (cb) {
  if (this._closed) {
    return process.nextTick(function(){
      return cb(new Error('Pool is closed.'));
    });
  }

  var connection;

  if (this._freeConnections.length > 0) {
    connection = this._freeConnections.shift();

    return process.nextTick(function(){
      return cb(null, connection);
    });
  }

  if (this.config.connectionLimit === 0 || this._allConnections.length < this.config.connectionLimit) {
    connection = new Kinesis(this, this.options);
    this._allConnections.push(connection);

    return cb(null, connection);
  }

  if (!this.config.waitForConnections) {
    return process.nextTick(function(){
      return cb(new Error('No connections available.'));
    });
  }

  if (this.config.queueLimit && this._connectionQueue.length >= this.config.queueLimit) {
    return cb(new Error('Queue limit reached.'));
  }

  this._connectionQueue.push(cb);
};

Pool.prototype.releaseConnection = function (connection) {
  var cb;
  this._count++;
  if (!connection._pool) {
    // The connection has been removed from the pool and is no longer good.
    if (this._connectionQueue.length) {
      cb = this._connectionQueue.shift();

      process.nextTick(this.getConnection.bind(this, cb));
    }
  } else if (this._connectionQueue.length) {
    cb = this._connectionQueue.shift();
    process.nextTick(cb.bind(null, null, connection));
  } else {
    this._freeConnections.push(connection);
  }
};